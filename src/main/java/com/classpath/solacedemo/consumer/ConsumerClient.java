package com.classpath.solacedemo.consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class ConsumerClient {

    @Bean
    public Consumer<String> process(){
        return (quote) -> {
            System.out.println(" Inside the consumer function");
            System.out.println(quote);
            };
        }
    }