package com.classpath.solacedemo.processor;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ProcessorClient {

    @Bean
    public Function<String, String> transform(){
        return (inputStr) ->{
            System.out.println("Inside the transform function");
            System.out.println(inputStr);
          return  inputStr.toUpperCase();
        };
    }

    @Bean
    public Function<Double, Double> convertFromUSDtoINR(){
        return usd -> {
            System.out.println("Inside the transfrom function of USD to INR : "+ usd * 75);
            return usd * 75;
        };
    }

    @Bean
    public Function<Double, String> printCurrencyConvertion(){
        return inr -> "Value in INR :: "+ inr;

    }
}