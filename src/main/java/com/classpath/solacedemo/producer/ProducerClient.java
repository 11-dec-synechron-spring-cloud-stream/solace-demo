package com.classpath.solacedemo.producer;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
public class ProducerClient {

    @Bean
    public Supplier<String> generate(){
        return () -> "Stock value is :: " + String.valueOf(Math.round(Math.random() * 18888));
    }

    @Bean
    public Supplier<Double> generateUSD(){
        return () -> Math.random() * 18888;
    }
}